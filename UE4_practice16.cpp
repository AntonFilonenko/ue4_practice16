﻿#include <iostream>
#include <time.h>

int main()
{
    const int N = 10;
    int a[N][N];

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            a[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << a[i][j] << " ";
        }
        std::cout << "\n";
    }

    int CurrenDay = buf.tm_mday;
    int MyRow = CurrenDay % N;
    int MySum = 0;

    for (int i = 0; i < N; i++)
    {
        MySum = MySum + a[MyRow][i];
    }
    std::cout << "Curren day: " << CurrenDay << "\n";
    std::cout << "My row: " << MyRow << "\n";
    std::cout << "My sum: " << MySum << "\n";


}
